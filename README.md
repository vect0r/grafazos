## Grafazos: Grafana dashboards for Tezos node monitoring.

Grafazos allows to generate dashboards from jsonnet programms.

Ressources:

 - Graphonnet: https://github.com/grafana/grafonnet-lib
 - Jsonnet: https://jsonnet.org/

Tools:

 - Emacs mode: https://github.com/mgyucht/jsonnet-mode
