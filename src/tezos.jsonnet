local grafana = import '../vendors/grafonnet-lib/grafonnet/grafana.libsonnet';
local dashboard = grafana.dashboard;
local template = grafana.template;
local singlestat = grafana.singlestat;
local graphPanel = grafana.graphPanel;
local prometheus = grafana.prometheus;

###
# Tezos related stats
###

{
  buildInfo:
    singlestat.new(
      title='Node release version (TODO)',
      datasource='Prometheus',
      format='none',
      valueName='name',
    ).addTarget(
      prometheus.target(
	'prometheus_build_info',
	legendFormat='{{ version }}',
      )
    ),

  headLevel:
    singlestat.new(
      title='Current head level',
      datasource='Prometheus',
      format='none',
      valueName='max',
    ).addTarget(
      prometheus.target(
	'tezos_metrics_chain_header_level',
	legendFormat='current head level',
      )
    ),

  headCycleLevel:
    singlestat.new(
      title='Current cycle',
      datasource='Prometheus',
      format='none',
      valueName='max',
    ).addTarget(
      prometheus.target(
	'tezos_metrics_chain_header_cycle',
	legendFormat='Current cycle',
      )
    ),

  headHistory:
    local head = 'Head level';
  graphPanel.new(
    title='Head level history',
    datasource='Prometheus',
    linewidth=1,
    format='none',
    aliasColors={
      [head]: 'light-green',
    },
  ).addTarget(
    prometheus.target(
      'tezos_metrics_chain_header_level',
      legendFormat=head,
    )
  ),

  invalidBlocksHistory:
    local blocks = 'Invalid blocks';
  graphPanel.new(
    title='Invalid blocks history',
    datasource='Prometheus',
    linewidth=1,
    format='none',
    aliasColors={
      [blocks]: 'light-green',
    },
  ).addTarget(
    prometheus.target(
      'tezos_metrics_chain_invalid_blocks',
      legendFormat=blocks,
    )
  ),

  storage:
    local total = 'Total';
  local context = 'Context store';
  local context_index = 'Context index';
  local store = 'Store';
  graphPanel.new(
    title='Storage',
    datasource='Prometheus',
    linewidth=1,
    format='mbytes',
    aliasColors={
      [total]: 'light-green',
      [context]: 'light-orange',
      [context_index]: 'light-yellow',
      [store]: 'light-blue',
    },
  ).addTarget(
    prometheus.target(
      'tezos_metrics_storage_total',
      legendFormat=total,
    )
  ).addTarget(
    prometheus.target(
      'tezos_metrics_storage_context',
      legendFormat=context,
    )
  ).addTarget(
    prometheus.target(
      'tezos_metrics_storage_index',
      legendFormat=context_index,
    )
  ).addTarget(
    prometheus.target(
      'tezos_metrics_storage_store',
      legendFormat=store,
    )
  ),

  p2p_peers:
    local accepted='Accepted peers';
  local disconnected='Disconnected peers';
  local running='Running peers';
  graphPanel.new(
    title='P2P peers connections',
    datasource='Prometheus',
    linewidth=1,
    format='none',
    aliasColors={
      [accepted]: 'light-green',
      [disconnected]: 'light-yellow',
      [running]: 'light-blue',
    },
  ).addTarget(
    prometheus.target(
      'tezos_metrics_p2p_peers_accepted',
      legendFormat=accepted,
    )
  ).addTarget(
    prometheus.target(
      'tezos_metrics_p2p_peers_disconnected',
      legendFormat=disconnected,
    )
  ).addTarget(
    prometheus.target(
      'tezos_metrics_p2p_peers_running',
      legendFormat=running,
    )
  ),

  p2p_points:
    local accepted='Accepted points';
  local disconnected='Disconnected points';
  local running='Running points';
  graphPanel.new(
    title='P2P points connections',
    datasource='Prometheus',
    linewidth=1,
    format='none',
    aliasColors={
      [accepted]: 'light-green',
      [disconnected]: 'light-yellow',
      [running]: 'light-blue',
    },
  ).addTarget(
    prometheus.target(
      'tezos_metrics_p2p_points_accepted',
      legendFormat=accepted,
    )
  ).addTarget(
    prometheus.target(
      'tezos_metrics_p2p_points_disconnected',
      legendFormat=disconnected,
    )
  ).addTarget(
    prometheus.target(
      'tezos_metrics_p2p_points_running',
      legendFormat=running,
    )
  ),

  workersRequests:
    local blockvalidator='Block validator';
  local chainprevalidator='Chain prevalidator';
  local chainvalidators='Chain validators';
  graphPanel.new(
    title='Pending workers requests',
    datasource='Prometheus',
    linewidth=1,
    format='none',
    aliasColors={
      [blockvalidator]: 'light-green',
      [chainprevalidator]: 'light-yellow',
      [chainvalidators]: 'light-blue',
    },
  ).addTarget(
    prometheus.target(
      'tezos_metrics_workers_block_validator',
      legendFormat=blockvalidator,
    )
  ).addTarget(
    prometheus.target(
      'tezos_metrics_workers_main_chain_prevalidators',
      legendFormat=chainprevalidator,
    )
  ).addTarget(
    prometheus.target(
      'tezos_metrics_workers_main_chain_validators',
      legendFormat=chainvalidators,
    )
  ),

### GC

  gcOperations:
    local minor='Minor collections';
  local major='Major collections';
  local compact='Heap compactions';
  graphPanel.new(
    title='CG maintenance operations',
    datasource='Prometheus',
    linewidth=1,
    format='none',
    aliasColors={
      [minor]: 'light-green',
      [major]: 'light-yellow',
      [compact]: 'light-blue',
    },
  ).addTarget(
    prometheus.target(
      'ocaml_gc_minor_collections',
      legendFormat=minor,
    )
  ).addTarget(
    prometheus.target(
      'ocaml_gc_major_collections',
      legendFormat=major,
    )
  ).addTarget(
    prometheus.target(
      'ocaml_gc_major_compactions',
      legendFormat=compact,
    )
  ),

  gcMajorHeap:
  local major='Major heap';
  local top='Top major heap';
  graphPanel.new(
    title='CG minor and mjor word sizes',
    datasource='Prometheus',
    linewidth=1,
    format='bytes',
    aliasColors={
      [major]: 'light-green',
      [top]: 'light-blue',
    },
  ).addTarget(
    prometheus.target(
      'ocaml_gc_heap_words',
      legendFormat=major,
    )
  ).addTarget(
    prometheus.target(
      'ocaml_gc_top_heap_words',
      legendFormat=top,
    )
  ),

}


