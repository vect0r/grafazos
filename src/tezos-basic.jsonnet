local grafana = import '../vendors/grafonnet-lib/grafonnet/grafana.libsonnet';
local dashboard = grafana.dashboard;
local template = grafana.template;
local singlestat = grafana.singlestat;
local graphPanel = grafana.graphPanel;
local prometheus = grafana.prometheus;

local tezos = import './tezos.jsonnet';

###
# Hardware relates stats
###

local ios =
local reads='reads';
local writes='writes';
  graphPanel.new(
    title='IOs',
    datasource='Prometheus',
    linewidth=1,
    format='kbytes',
    aliasColors={
      [reads]: 'light-green',
      [writes]: 'light-yellow',
    },
  ).addTarget(
    prometheus.target(
      'netdata_apps_lreads_KiB_persec_average{dimension="tezos"}',
      legendFormat=reads,
    )
  ).addTarget(
    prometheus.target(
      'netdata_apps_lwrites_KiB_persec_average{dimension="tezos"}',
      legendFormat=writes,
    )
  );

local cpu =
local load='Cpu load';
  graphPanel.new(
    title='Cpu actitvity',
    datasource='Prometheus',
    linewidth=1,
    format='percent',
    aliasColors={
      [load]: 'light-green',
    },
  ).addTarget(
    prometheus.target(
      'netdata_apps_cpu_percentage_average{dimension="tezos"}',
      legendFormat=load,
    )
  );

local ram =
local usage='Memory usage';
  graphPanel.new(
    title='Memory usage',
    datasource='Prometheus',
    linewidth=1,
    format='mbytes',
    aliasColors={
      [usage]: 'light-green',
    },
  ).addTarget(
    prometheus.target(
      'netdata_apps_mem_MiB_average{dimension="tezos"}',
      legendFormat=usage,
    )
  );

local fileDescriptors =
local total='All fds';
local sockets='Sockets';
local files='Files';
local pipes='Pipes';
  graphPanel.new(
    title='File descriptors',
    datasource='Prometheus',
    linewidth=1,
    format='none',
    aliasColors={
      [total]: 'light-green',
      [sockets]: 'light-yellow',
      [files]: 'light-blue',
      [pipes]: 'light-orange',
    },
  ).addTarget(
    prometheus.target(
      'sum(netdata_apps_pipes_open_pipes_average{dimension="tezos"}) + sum(netdata_apps_files_open_files_average{dimension="tezos"}) + sum(netdata_apps_sockets_open_sockets_average{dimension="tezos"})',
      legendFormat=total,
    )
  ).addTarget(
    prometheus.target(
      'netdata_apps_sockets_open_sockets_average{dimension="tezos"}',
      legendFormat=sockets,
    )
  ).addTarget(
    prometheus.target(
      'netdata_apps_files_open_files_average{dimension="tezos"}',
      legendFormat=files,
    )
  ).addTarget(
    prometheus.target(
      'netdata_apps_pipes_open_pipes_average{dimension="tezos"}',
      legendFormat=pipes,
    )
  );



####
# Grafana main stuffs
###
dashboard.new(
  'Tezos node test',
  tags=['tezos','storage'],
  schemaVersion=18,
  editable=true,
  time_from='now-3h',
  refresh='',
)

#Node a grid is 24 slots wide
.addPanels(
  [
    #Line 0 -> h=3
    tezos.buildInfo { gridPos: { h: 3, w: 24, x: 0, y: 0 } },

    #Line 1 -> h=6
    tezos.headLevel { gridPos: { h: 3, w: 5, x: 0, y: 3 } }, tezos.headCycleLevel { gridPos: { h: 3, w: 5, x: 0, y: 6 } }, tezos.headHistory { gridPos: { h: 6, w:19, x: 6, y: 3 } },

    #Line 2 -> h=8
    tezos.storage { gridPos: { h:8, w:12, x:0, y:7 } }, ios { gridPos: { h:8, w:12, x:12, y:7 } },

    #Lien 3 -> h=8
    cpu { gridPos: { h:8, w:12, x:0, y:15 } }, ram { gridPos: { h:8, w:12, x:12, y:15 } },

    #Line 4 -> h=8
    fileDescriptors { gridPos: { h:8, w:12, x:0, y:23 } }, tezos.p2p_peers { gridPos: { h:8, w:12, x:12, y:23 } },

    #Line 4 -> h=8
    tezos.p2p_points { gridPos: { h:8, w:12, x:0, y:31 } }, tezos.p2p_peers { gridPos: { h:8, w:12, x:12, y:31 } },

    #Line 4 -> h=8
    tezos.invalidBlocksHistory { gridPos: { h:8, w:12, x:0, y:39 } }, tezos.workersRequests { gridPos: { h:8, w:12, x:12, y:39 } },

    #Line 4 -> h=8
    tezos.gcOperations { gridPos: { h:8, w:12, x:0, y:47 } }, tezos.gcMajorHeap { gridPos: { h:8, w:12, x:12, y:47 } },
  ]
)
